/* Задача 1:
перепишите if с использованием оператора "?"
if (a + b) < 4 {
    result = "Мало";
} else {
    result = "Много";
}; */

let a = parseInt(prompt("Введите значение a", 0));
let b = parseInt(prompt("Введите значение b", 0));
let result;

if (((a + b) < 4) ? result = "Мало" : result = "Много");
document.write(result, "<hr/>");

/* Задача 2:
перепишите if...else с использованием нескольких операторов "?"
var message;

if (login == "Вася"){
    message = "Привет";
} else if login == "Директор"){
    message = "Здравствуйте";
} else if login == ""){
    message = "Нет логина";
} else {
    message = "";
} */

let login = prompt("Введите свой логин", "Вася");
let message;

if (login == "Вася" ? message = "Привет" : "");
if (login == "Директор" ? message = "Здравствуйте" : "");
if (login == "" ? message = "Нет логина" : "");

document.write(login + " " + message + "<hr/>");

/* Задача 3:
Дано два числа C и D где (A "<" B). Выведите на экран сумму всех чисел, расположенных в числовом 
промежутке от C до D. Выведите на экран все нечетные значения , расположенные в промежутке от C до D. */

let c = parseInt(prompt("Введите значение c", 0));
let d = parseInt(prompt("Введите значение d", 0));
let sum = 0;
let list = "";

if (c < d) {
    
    while (c < d) {
        sum += c;
        if (c % 2 !== 0) {
            list += " " + c;
            c++; /* в условие будут браться числа между c и d, включая c, но НЕ включая d */
        };
    };
};

document.write("Сумма чисел от c до d равна: " + sum + "<br/>");
if (list.length > 0) {
    document.write("Нечетные числа от c до d: " + list + "<hr/>");
} else {
    document.write("В числовом промежутке от c до d нечетных чисел не найдено" + "<hr/>");
};

/* Задача 4:
Используя циклы, нарисуйте в браузере с помощью пробелов () и звездочек (*):
- прямоугольник;
- прямоугольный треугольник;
- равносторонний треугольник;
- ромб. */

document.write("ПРЯМОУГОЛЬНИК" + "<br/>" + "<br/>");

for (var i = 0; i < 6; i++) {
    for (let e = 0; e < 25; e++) {
        document.write("");
    }
    for (let f = 0; f < 25; f++) {
        document.write("*");
    }
    document.write("<br/>");
}
document.write("<hr/>");

document.write("ПРЯМОУГОЛЬНЫЙ ТРЕУГОЛЬНИК" + "<br/>");

for (var i = 0; i < 10; i++) {

    for (let f = 0; f < i; f++) {
        document.write("*&nbsp;");
    };

    document.write("<br/>");
};
document.write("<hr/>");

document.write("РАВНОСТОРОННИЙ ТРЕУГОЛЬНИК" + "<br/>");

for (var i = 0; i < 10; i++) {

    for (let g = 10; g > i; g--) {
        document.write("&nbsp;&nbsp;&nbsp;"); /* приходится равнять пробелами, потому что размер пробела и звехдочки разный */
    }
    for (let h = 1; h < (3 * i); h++) {
        document.write("*");
    }

    document.write("<br/>");
};
document.write("<hr/>");

document.write("РОМБ" + "<br/>" + "<br/>");

for (var i = 0; i < 7; i++) {
    for (let k = (0 + i); k < 7; k++) {
        document.write("&nbsp;&nbsp;");
    }
    for (let l = (i + 1); l > 0; l--) {
        document.write("**");
    }

    document.write("<br/>");
};

for (var i = 0; i < 7; i++) {
    for (let k = (1 + i); k > 0; k--) {
        document.write("&nbsp;&nbsp;");
    }
    for (let l = (i + 0); l < 7; l++) {
        document.write("**");
    }

    document.write("<br/>");
};
document.write("<hr/>");